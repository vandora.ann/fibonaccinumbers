using NUnit.Framework;

namespace FibonacciNumbers.Tests
{
    public class FibonacciNumbersTests
    {
        [Test]
        public void FibonacciNumbersTest1()
        {
            var fibonacciNumbers = new FibonacciNumbers(-5, 5);
            var fibonacciTest = new[] { 5, -3, 2, -1, 1, 0, 1, 1, 2, 3, 5 };
            Assert.AreEqual(fibonacciNumbers.Numbers, fibonacciTest);
        }

        [Test]
        public void FibonacciNumbersTest2()
        {
            var fibonacciNumbers = new FibonacciNumbers(5, 5);
            var fibonacciTest = new[] { 5 };
            Assert.AreEqual(fibonacciNumbers.Numbers, fibonacciTest);
        }

        [Test]
        public void FibonacciNumbersTest3()
        {
            var fibonacciNumbers = new FibonacciNumbers(-46, 46);
            Assert.AreEqual(fibonacciNumbers.Numbers.Length, 93);
            Assert.AreEqual(fibonacciNumbers.Numbers[0], -1836311903);
            Assert.AreEqual(fibonacciNumbers.Numbers[46], 0);
            Assert.AreEqual(fibonacciNumbers.Numbers[92], 1836311903);
        }

        [Test]
        public void FibonacciDirectOrderTest()
        {
            var fibonacciTest = new[] { 5, -3, 2, -1, 1, 0, 1, 1, 2, 3, 5 };
            var factoryFibonacci = new FactoryFibonacci();
            var fibonacci = factoryFibonacci.GetFibonacciEnumerable(1);
            var iterator = fibonacci.Fibonacci(-5, 5);
            var i = 0;

            while (iterator.HasNext())
            {
                Assert.AreEqual(iterator.Next(), fibonacciTest[i++]);
            }
        }

        [Test]
        public void FibonacciReverseOrderTest()
        {
            var fibonacciTest = new[] { 5, -3, 2, -1, 1, 0, 1, 1, 2, 3, 5 };
            var factoryFibonacci = new FactoryFibonacci();
            var fibonacci = factoryFibonacci.GetFibonacciEnumerable(2);
            var iterator = fibonacci.Fibonacci(-5, 5);
            var i = 10;

            while (iterator.HasNext())
            {
                Assert.AreEqual(iterator.Next(), fibonacciTest[i--]);
            }
        }
    }
}