﻿using System;

namespace FibonacciNumbers
{
    internal class DirectOrderFactory : IFibonacciFactory
    {
        public IFibonacciEnumerable GetEnumerable()
        {
            return new DirectOrderFibonacci();
        }
    }
}