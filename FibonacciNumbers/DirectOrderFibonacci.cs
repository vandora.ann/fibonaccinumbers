﻿namespace FibonacciNumbers
{
    public class DirectOrderFibonacci : IFibonacciEnumerable
    {
        public IElementIterator Fibonacci(int begin, int end)
        {
            return new FibonacciElementIterator(begin, end).CreateNumerator();
        }
    }
}
