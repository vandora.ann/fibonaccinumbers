﻿namespace FibonacciNumbers
{
    public class FactoryFibonacci
    {
        private DirectOrderFactory _directOrder = null;
        private ReverseOrderFactory _reverseOrder = null;

        public FactoryFibonacci()
        {
            _directOrder = new DirectOrderFactory();
            _reverseOrder = new ReverseOrderFactory();
        }
        public IFibonacciEnumerable GetFibonacciEnumerable(int type)
        {
            IFibonacciEnumerable fibonacci = null;

            switch (type)
            {
                case 1:
                    fibonacci = _directOrder.GetEnumerable();
                    break;
                case 2:
                    fibonacci = _reverseOrder.GetEnumerable();
                    break;
            }

            return fibonacci;
        }
    }
}
