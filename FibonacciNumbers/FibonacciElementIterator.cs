﻿namespace FibonacciNumbers
{
    public class FibonacciElementIterator : IElementNumerable
    {
        private int[] _numbers;
        private readonly int _begin;
        private readonly int _end;
        
        public int this[int index] => _numbers[index];

        public FibonacciElementIterator(int begin, int end)
        {
            _begin = begin;
            _end = end;           
        }

        public int Count
        {
            get { return _numbers.Length; }
        }

        public IElementIterator CreateNumerator()
        {
            _numbers = (new FibonacciNumbers(_begin, _end)).Numbers;
            return new FibonacciNumerator(this);
        }

        public IElementIterator CreateBackNumerator()
        {
            var numbers = (new FibonacciNumbers( _begin, _end)).Numbers;
            var length = numbers.Length;
            _numbers = new int[length];

            for (var i = 0; i < length; ++i)
            {
                _numbers[i] = numbers[length - 1 - i];
            }
            return new FibonacciNumerator(this);
        }
    }
}
