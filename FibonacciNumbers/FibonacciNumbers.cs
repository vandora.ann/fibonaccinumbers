﻿using System;
using System.Collections.Generic;

namespace FibonacciNumbers
{
    internal class FibonacciNumbers
    {
        private readonly double _sqrt5 = Math.Sqrt(5);
        private readonly double _a = (1 + Math.Sqrt(5)) * 0.5;

        public readonly int[] Numbers;

        public FibonacciNumbers(int beginIndex, int endIndex)
        {
            Numbers = FibonacciForInterval(beginIndex, endIndex);
        }

        private int[] FibonacciForInterval(int begin, int end)
        {
            if (end < begin)
                throw new ArgumentException();

            var n = begin;
            var fibonaccis = new List<int>();
            var next = FindNumber(n++);
            fibonaccis.Add(next);

            if (n >= end)
                return fibonaccis.ToArray();

            next = FindNumber(n);
            
            while (n++ <= end)
            {
                fibonaccis.Add(next);
                var count = fibonaccis.Count;
                next = fibonaccis[count - 2] + fibonaccis[count - 1];
            }

            return fibonaccis.ToArray();
        }

        ////private int[] FibonacciForInterval(int begin, int end)
        ////{
        ////    if (end <= begin)
        ////        throw new ArgumentException();

        ////    var n = begin == 0 ? 0 : GetN(begin);
        ////    var fibonaccis = new List<int>();
        ////    var next = FindNumber(n);

        ////    if (next <= end)
        ////        fibonaccis.Add(next);
        ////    else return new int[0];

        ////    next = FindNumber(n + 1);

        ////    if (next <= end)
        ////        fibonaccis.Add(next);
        ////    else return fibonaccis.ToArray();

        ////    next = fibonaccis[0] + fibonaccis[1];

        ////    while (next <= end)
        ////    {
        ////        fibonaccis.Add(next);
        ////        var count = fibonaccis.Count;
        ////        next = fibonaccis[count - 2] + fibonaccis[count - 1];
        ////    }

        ////    return fibonaccis.ToArray();
        ////}

        private int FindNumber(int n)
        {
            var result = (int)Math.Round(Math.Pow(_a, Math.Abs(n)) / _sqrt5);

            if (n >= 0)
                return result;

            var i = n % 2 == 0 ? -1 : 1;
            return i * result;
        }

        private int GetN(int number)
        {
            var i = number > 0 ? 1 : -1;
            var n = i * Math.Round(Math.Log(_sqrt5 * i *number) / Math.Log(_a));
            return (int)n;
        }
    }
}
