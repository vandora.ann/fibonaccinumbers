﻿namespace FibonacciNumbers
{
    public class FibonacciNumerator : IElementIterator
    {
        private IElementNumerable _aggregate;
        private int _index;

        public FibonacciNumerator(IElementNumerable element)
        {
            _index =  0;
            _aggregate = element;
        }

        public bool HasNext()
        {
            return _index < _aggregate.Count;
        }

        public long Next()
        {
            return _aggregate[_index++];
        }
    }
}
