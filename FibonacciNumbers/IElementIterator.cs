﻿namespace FibonacciNumbers
{
    public interface IElementIterator
    {
        bool HasNext();
        long Next();
    }
}