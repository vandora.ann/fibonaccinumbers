﻿namespace FibonacciNumbers
{
    public interface IElementNumerable
    {
        IElementIterator CreateNumerator();
        IElementIterator CreateBackNumerator();

        int Count { get; }
        int this[int index] { get; }
    }
}