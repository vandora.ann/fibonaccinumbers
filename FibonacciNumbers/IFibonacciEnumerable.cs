﻿namespace FibonacciNumbers
{
    public interface IFibonacciEnumerable
    {
        public IElementIterator Fibonacci(int begin, int end);
    }
}