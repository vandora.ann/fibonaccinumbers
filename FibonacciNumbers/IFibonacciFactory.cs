﻿namespace FibonacciNumbers
{
    public interface IFibonacciFactory
    {
        public IFibonacciEnumerable GetEnumerable();
    }
}