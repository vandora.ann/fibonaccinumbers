﻿using System;
using System.IO;
using System.Text;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("FibonacciNumbers.Tests")]


namespace FibonacciNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите начала интервала чисел фибоначий (min -46):");
            var console = ParseToInt(Console.ReadLine());
            var begin = console < -46 ? -46 : console;

            Console.WriteLine("Введите конец интервала чисел фибоначий (max 46):");
            console = ParseToInt(Console.ReadLine());
            var end = console > 46 ? 46 : console;

            Console.WriteLine("Введите: 1 - прямой порядок, 2 - обратный порядок:");
            console = ParseToInt(Console.ReadLine());
            var type = console == 1 || console == 2 ? console : 1;

            Console.WriteLine("Путь к файлу с результатом (путь по умолчанию: C\\tests\\fibonacci.txt):");
            var readLine = Console.ReadLine();
            var filePath = File.Exists(readLine) ? readLine : Path.Combine("C:", "tests", "fibonacci.txt");

            var factoryFibonacci = new FactoryFibonacci();
            var fibonacci = factoryFibonacci.GetFibonacciEnumerable(type);
            if (fibonacci != null)
            {
                try
                {
                    var iterator = fibonacci.Fibonacci(begin, end);
                    var result = new StringBuilder();
                    while (iterator.HasNext())
                    {
                        var fib = iterator.Next();
                        result.Append(fib);
                        result.Append(" ");
                    }

                    File.WriteAllText(filePath, result.ToString().Trim());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

        }

        private static int ParseToInt(string text)
        {
            try
            {
                return Convert.ToInt32(text);
            }
            catch
            {
                return 0;
            }
        }
    }
}
