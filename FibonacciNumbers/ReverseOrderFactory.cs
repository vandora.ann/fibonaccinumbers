﻿using System;

namespace FibonacciNumbers
{
    internal class ReverseOrderFactory : IFibonacciFactory
    {
        public IFibonacciEnumerable GetEnumerable()
        {
            return new ReverseOrderFibonacci();
        }
    }
}