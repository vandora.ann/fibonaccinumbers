﻿namespace FibonacciNumbers
{
    public class ReverseOrderFibonacci : IFibonacciEnumerable
    {
        public IElementIterator Fibonacci(int begin, int end)
        {
            return new FibonacciElementIterator(begin, end).CreateBackNumerator();
        }
    }
}
